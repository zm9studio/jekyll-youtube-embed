Gem::Specification.new do |s|
  s.name        = "jekyll-youtube-embed"
  s.version     = "0.0.4"
  s.summary     = "Embed youtube in Jekyll site markdown post"
  s.description = "A simple Jekyll YouTube embed gem. Syntax {% youtube video_id %}"
  s.authors     = ["zm9studio"]  
  s.files       = ["lib/jekyll-youtube-embed.rb"]
  s.homepage    = "https://gitlab.com/zm9studio/jekyll-youtube-embed"
  s.license     = "MIT"
  s.metadata = {
    "documentation_uri" => "https://gitlab.com/zm9studio/jekyll-youtube-embed",
    "homepage_uri"      => "https://gitlab.com/zm9studio/jekyll-youtube-embed"
  }
end