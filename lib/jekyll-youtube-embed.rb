module Jekyll
  class YouTubeTag < Liquid::Tag

    def initialize(tag_name, text, tokens)
      super
      @text = text.strip
    end

    def render(context)
      "<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/#{@text}\" frameborder=\"0\" allowfullscreen></iframe>"
    end
  end
end

Liquid::Template.register_tag('youtube', Jekyll::YouTubeTag)
