# Jekyll YouTube Plugin

This is a simple Jekyll plugin that allows you to embed YouTube videos in your Jekyll site posts.

## Installation

Add the gem to the _config.yml file 
```yml
plugins:
  jekyll-youtube-embed
```

## Usage

```
{% youtube rt1mRnRp79A %}
```

To embed a YouTube video in a post, use the `youtube` tag and pass in the YouTube video ID, usually found after `v=` in the video URL.

Here's an example:
If the YouTube url is `https://www.youtube.com/watch?v=rt1mRnRp79A`, `rt1mRnRp79A` is the `id`

